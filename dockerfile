FROM node:18-alpine
# Diretório de trabalho dentro do contêiner
WORKDIR /app
# Copiar os arquivos de dependências para o diretório de trabalho
COPY package.json package-lock.json /app/
# Instalar as dependências
RUN npm ci --only=production
# Copiar o código-fonte da aplicação para o diretório de trabalho
COPY . /app
# Expor a porta da aplicação (caso sua aplicação utilize uma porta específica)
EXPOSE 3000
# Comando para iniciar a aplicação
CMD ["node", "server.js"]